﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DockerDeployOnServer.DataAccess;
using DockerDeployOnServer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DockerDeployOnServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UsersContext context;

        public AuthController(UsersContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> LogIn(User user)
        {
            foreach (var item in await context.Users.ToListAsync())
            {
                if (item.Login == user.Login && item.Password == user.Password)
                {
                    return Ok("Success!");
                }
            }

            return Unauthorized();
        }

        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

    }
}
